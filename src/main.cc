#include <ctime>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <random>
#include <algorithm>

#include "options/Options.hpp"
#include "io/FastaReader.hpp"
#include "ds/InsertRecord.hpp"


void sampleInserts( const Options &opt ) 
{
	time_t t1 = time(NULL);
	
	Sequence ref_seq;
	FastaReader reader( opt.input() );

	int32_t max_insert = opt.meanInsert() + opt.maxDev();
	int32_t min_insert = opt.meanInsert() - opt.maxDev();
	double  min_coverage = opt.minCoverage();

	size_t ref_id = 0;

	std::vector<Sequence> collection;
	std::vector<InsertRecord> inserts;
	
	while( reader.next_sequence(ref_seq) ) // for each sequence in the reference multifasta
	{
		size_t ref_len = ref_seq.size();
		size_t gen_len = 0;

		if( ref_len < max_insert ) continue; // sample exclusively from sequences which are long enough

		collection.push_back(ref_seq);
		
		// initialize insert-size and sampled-position distributions
		
		std::random_device ins_rd;
		std::mt19937_64 ins_gen( ins_rd() );
    	std::uniform_int_distribution<int> ins_dist( min_insert, max_insert );

    	std::random_device pos_rd;
    	std::mt19937_64 pos_gen( pos_rd() );
    	std::uniform_int_distribution<int> pos_dist( 0, ref_len-min_insert );

		// sample inserts until minimum coverage is reached

		while( gen_len / static_cast<double>(ref_len) < min_coverage )
		{
			size_t pos = pos_dist(pos_gen);     // pick a random position in the current reference sequence
			size_t ins_len = ins_dist(ins_gen); // pick a random insert size

			if( pos + ins_len < ref_len + 1 ) // valid insert
			{
				gen_len += ins_len;
				inserts.emplace_back( ref_id, pos, ins_len );
			}
		}

		ref_id++;
	}

	size_t inserts_size = inserts.size();
	std::cout << inserts_size << " inserts have been sampled." << std::endl;

	// sort inserts

	std::sort( inserts.begin(), inserts.end() );

	// shuffle inserts

	std::random_device rd;
	std::mt19937_64 randEng(rd());
	std::shuffle( inserts.begin(), inserts.end(), randEng );

	// write pools to file
	
	size_t pool_size = static_cast<size_t>(opt.poolSize());
	size_t pool_id = 0;
	
	for( size_t ins_id=0; ins_id < inserts_size; ins_id += pool_size )
	{
		// keep last pool only if it full at least half the maximum size
		if( ins_id + pool_size >= inserts_size and inserts_size - ins_id < pool_size/2 ) break;

		std::stringstream of_ss;
		of_ss << opt.prefix() << pool_id << ".fasta";
		std::ofstream ofs( of_ss.str().c_str() );

		size_t seq_id = 0;

		while( seq_id < pool_size and ins_id + seq_id < inserts_size )
		{
			const InsertRecord &rec = inserts.at( ins_id+seq_id );
			const Sequence &ref_seq = collection.at( rec.id() );

			Sequence ins_seq = ref_seq.subseq( rec.pos(), rec.pos()+rec.len()-1 );
			
			std::stringstream ins_seq_ss;
			ins_seq_ss << seq_id << " " << ref_seq.str_id() << " [" << rec.pos() << "," << rec.pos() + rec.len() << ")";
			ins_seq.set_str_id( ins_seq_ss.str() );

			ofs << ins_seq << '\n';

			seq_id++;
		}

		ofs.close();

		pool_id++;
	}
}


int main(int argc, char *argv[]) 
{
	// retrieve options from command line
	Options options(argc,argv);
	
	// sample long-insert clones
	sampleInserts(options);
	
	return EXIT_SUCCESS;
}
