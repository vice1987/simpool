#ifndef FASTA_READER_HPP
#define	FASTA_READER_HPP

#include <string>
#include <fstream>

#include "ds/Sequence.hpp"
#include "errors/FastaReaderException.hpp"


class FastaReader
{

private:

	std::string   m_filename;
	std::ifstream m_ifs;

	bool get_next_id( Sequence& seq ); // throws FastaReaderException
	bool get_next_sequence( Sequence& seq ); // throws FastaReaderException

public:

    FastaReader( const std::string &filename ) // throws FastaReaderException
    {
        this->open( filename.c_str() );
    }

	FastaReader( const char *filename ) // throws FastaReaderException
    {
        this->open(filename);
    }

    const std::string& filename() const;
    const std::ifstream& ifs() const;

    void open( const std::string &filename ); // throws FastaReaderException
    void open( const char *filename ); // throws FastaReaderException
	void close();

    void rewind();

	bool next_sequence( Sequence& seq ); // throws FastaReaderException

}; // class FastaReader


#endif	//FASTA_READER_HPP
