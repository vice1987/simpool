#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <cstdlib>
#include <string>

class Options
{

private:
	
	// input options
	std::string m_input;
	
	// parameters
	double  m_min_cov;
	int32_t m_mean_insert;
	int32_t m_max_dev;
	int32_t m_pool_size;
	
	// output options
	std::string m_prefix;

protected:
	
	void set_defaults();

public:
		
	Options(int argc, char *argv[]);
	
	bool process(int argc, char *argv[]);

	inline const std::string& input() const { return m_input; }

	inline double  minCoverage() const { return m_min_cov; }
	inline int32_t meanInsert() const { return m_mean_insert; }
	inline int32_t maxDev() const { return m_max_dev; }
	inline int32_t poolSize() const { return m_pool_size; }

	inline const std::string& prefix() const { return m_prefix; }
	
}; // class Options

#endif // OPTIONS_HPP
