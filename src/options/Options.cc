#include "Options.hpp"

#include <iostream>

#include <boost/program_options.hpp>
namespace bpo = boost::program_options;

#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;


//*****************************************************************

Options::Options( int argc, char *argv[] )
{
	this->set_defaults();
	if( not this->process(argc,argv) ) exit(EXIT_FAILURE);
}

//*****************************************************************

void Options::set_defaults()
{
	m_input = "";

	m_min_cov = 8;
	m_mean_insert = 40000;
	m_max_dev = m_mean_insert*0.15; // 15% of the mean value
	m_pool_size = 80;

	m_prefix = "pool_";
}

//*****************************************************************

bool Options::process(int argc, char *argv[])
{
	bpo::variables_map vm;

	try
	{
		std::string desc_str = "\nsim-pool: sample long-insert clones from a reference multifasta.\n\nUsage";
		
		bpo::options_description desc(desc_str.c_str());
		desc.add_options()

		("help,h", "produce this help message\n")
		
		// input
		("input,i", bpo::value< std::string >(&m_input)->required(), "path of a FASTA file containing the reference sequences")
		
		// parameters
		("coverage,c",  bpo::value<double>(&m_min_cov)->required(),      "minimum coverage")
		("mean,m",      bpo::value<int32_t>(&m_mean_insert)->required(), "mean insert size")
		("max-dev,d",   bpo::value<int32_t>(&m_max_dev),                 "max deviation of insert size from the mean value")
		("pool-size,s", bpo::value<int32_t>(&m_pool_size)->required(),   "maximum number of inserts assigned to each pool")
		
		// output
		("prefix,p", bpo::value<std::string>(&m_prefix), "output prefix")
		;
		
		// parse command line
	
		bpo::store( bpo::parse_command_line(argc,argv,desc), vm );

		if( vm.count("help") )
        {
            std::cout << desc << "\n";
            return false;
        }

		bpo::notify(vm);
	} 
	catch( bpo::required_option error ) 
	{
		std::cerr << error.what() << std::endl;
		std::cerr << "try \"--help\" for help." << std::endl;
		exit(EXIT_FAILURE);
	}
	catch( bpo::error error ) 
	{
		std::cerr << error.what() << std::endl;
		std::cerr << "try \"--help\" for help." << std::endl;
		exit(EXIT_FAILURE);
	}

	bool failed = false;

	// check input file existance
	if( not bfs::exists(m_input.c_str()) )
	{
		std::cerr << "[error] input file \"" << m_input << "\" does not exist." << std::endl;
		failed = true;
	}

	// check coverage
	if( m_min_cov <= 0 )
	{
		std::cerr << "[error] minimum coverage must be a positive value." << std::endl;
		failed = true;
	}

	// check mean insert value
	if( m_mean_insert < 1 )
	{
		std::cerr << "[error] mean insert size must be a positive integer." << std::endl;
		failed = true;
	}

	if( vm.count("max-dev") == 0 ) // if max deviation is not provided set it as 15% of the mean-value
	{
		m_max_dev = static_cast<int32_t>(m_mean_insert * 0.15);
	}

	// check max deviation
	if( m_max_dev > m_mean_insert/2 )
	{
		m_max_dev = m_mean_insert/2;
		std::cerr << "[warning] max deviation cannot be greater than half of the mean value. max-dev set to " << m_max_dev << std::endl;
	}

	// check max pool size
	if( m_pool_size < 1 )
	{
		std::cerr << "[error] pool size must be a positive integer." << std::endl;
		failed = true;
	}

	return !failed; // true if no error occurred, false otherwise 
}
