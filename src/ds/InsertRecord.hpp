#ifndef INSERT_RECORD_HPP
#define INSERT_RECORD_HPP

class InsertRecord
{

private:

	size_t m_rid;
	size_t m_pos;
	size_t m_len;

public:

	InsertRecord( size_t rid, size_t pos, size_t len ) :
		m_rid(rid),
		m_pos(pos),
		m_len(len)
	{ }

	inline size_t id() const { return m_rid; }
	inline size_t pos() const { return m_pos; }
	inline size_t len() const { return m_len; }

	inline bool operator<( const InsertRecord& other ) const
	{
		return ( m_rid < other.m_rid || 
			     ( m_rid == other.m_rid and m_pos < other.m_pos ) || 
			     ( m_rid == other.m_rid and m_pos == other.m_pos and m_len < other.m_len) 
			   );
	}
};

#endif // INSERT_RECORD_HPP
