#include "Sequence.hpp"

#include <iostream>

#include "common/Constants.hpp"


Sequence::Sequence() : 
	m_pid(-1), 
	m_sid(-1), 
	m_str_id("") 
{ }


int32_t Sequence::pid() const
{
	return m_pid;
}


void Sequence::set_pid( int32_t pid )
{
	m_pid = pid;
}


int32_t Sequence::sid() const
{
	return m_sid;
}


void Sequence::set_sid( int32_t sid )
{
	m_sid = sid;
}


const std::string& Sequence::str_id() const
{
	return m_str_id;
}


void Sequence::set_str_id( const std::string& id )
{
	m_str_id = id;
}


base_t* Sequence::data()
{
	return m_seq.data();
}


const base_t* Sequence::data() const
{
	return m_seq.data();
}


size_t Sequence::size() const
{
	return m_seq.size();
}


size_t Sequence::length() const
{
	return m_seq.size();
}


base_t& Sequence::at( const size_t& i )
{
	return m_seq.at(i);
}


const base_t& Sequence::at( const size_t& i ) const
{
	return m_seq.at(i);
}


base_t& Sequence::operator[]( const size_t& i )
{
	return m_seq[i];
}


const base_t& Sequence::operator[]( const size_t& i ) const
{
	return m_seq[i];
}


void Sequence::push_back( base_t b )
{
	m_seq.push_back(b);
}


void Sequence::clear()
{
	m_pid = -1;
	m_str_id = "";
	m_seq.clear();
}

void Sequence::resize( size_t newsize )
{
	m_seq.resize( newsize, constants::BASE_N );
}


void Sequence::reverse_complement()
{
    size_t j = 0, i = m_seq.size();

	while( j+1 <= i )
	{
		i--;

		if( j+1 < i )
		{
			std::swap(m_seq[j],m_seq[i]);
			m_seq[j] = constants::base_rc[m_seq[j]];
			m_seq[i] = constants::base_rc[m_seq[i]];
		}
		else // j+1 == i
		{
			m_seq[j] = constants::base_rc[m_seq[j]];
		}

		j++;
	}
}


Sequence Sequence::subseq( int32_t start, int32_t end ) const
{
	Sequence out;

	// DEBUG: it should be removed in final release
	if( start < 0 || end < 0 || start >= m_seq.size() || end >= m_seq.size() )
	{
		std::cerr << "[error] cannot extract substring [" << start << "," << end << "] from sequence " << m_pid << "." << m_sid << " (len=" << m_seq.size() << ")" << std::endl;
		exit(1);
	}
	
	if( start <= end ) for( int32_t i = start; i <= end; i++ ) out.push_back( m_seq.at(i) );
		else for( int32_t i = start; i >= end; i-- ) out.push_back( constants::base_rc[m_seq.at(i)] );
	
	return out;
}


std::string Sequence::string() const
{
	std::string ret;
	ret.reserve( m_seq.size() );

	for( size_t i=0; i < m_seq.size(); ++i )
		ret.push_back( constants::base2char[m_seq[i]] );

	return ret;
}


std::ostream& operator<<( std::ostream &os, const Sequence &seq )
{
	size_t len = seq.length();

	os << '>' << seq.m_str_id << "\n";
	for( size_t i = 0; i < len; i++ )
	{
		base_t base = seq.m_seq[i];
		os << constants::base2char[base];
	}

	return os;
}
