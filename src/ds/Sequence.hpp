#ifndef SEQUENCE_HPP
#define	SEQUENCE_HPP

#include <cstdint>

#include <iosfwd>
#include <vector>
#include <string>

#include "common/Types.hpp"

class Sequence
{

private:

	int32_t m_pid; // pool id
	int32_t m_sid; // sequence id

    std::string m_str_id; // string identifier
	std::vector<base_t> m_seq; // Sequence

public:

	Sequence();
    Sequence( const Sequence& ) = default;
    Sequence( Sequence&& ) = default;

    Sequence& operator=( const Sequence& ) = default;
    Sequence& operator=( Sequence&& ) = default;

    int32_t pid() const;
	void set_pid( int32_t pid );

	int32_t sid() const;
	void set_sid( int32_t sid );

	const std::string& str_id() const;
	void set_str_id( const std::string& id );

	base_t* data();
	const base_t* data() const;

	size_t size() const;
	size_t length() const;

	base_t& at( const size_t& i );
	const base_t& at( const size_t& i ) const;

	base_t& operator[]( const size_t& i );
	const base_t& operator[]( const size_t& i ) const;

    void push_back( base_t b );

	void clear();

	void resize( size_t newsize );

    void reverse_complement();

    Sequence subseq( int32_t start, int32_t end ) const;

	std::string string() const;

	friend std::ostream& operator<<( std::ostream& os, const Sequence& seq );

}; // end class Sequence


#endif	// SEQUENCE_HPP
