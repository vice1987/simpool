#include "FastaReaderException.hpp"

#include <sstream>


FastaReaderException::FastaReaderException(const std::string &comment)
{
	std::stringstream ss;
	ss << "[FastaReaderException] " << comment << std::endl;
	m_comment = ss.str();
}


FastaReaderException::FastaReaderException(const std::string *comment)
{
	std::stringstream ss;
	ss << "[FastaReaderException] " << *comment << std::endl;
	m_comment = ss.str();
}


FastaReaderException::FastaReaderException(const char *comment)
{
	std::stringstream ss;
	ss << "[FastaReaderException] " << comment << std::endl;
	m_comment = ss.str();
}


FastaReaderException::~FastaReaderException() throw() {}


const char* FastaReaderException::what() const throw()
{
	return m_comment.c_str();
}
