/*
 * File:   utils.hpp
 * Author: riccardo
 *
 * Created on February 20, 2013, 12:42 PM
 */

#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <vector>
#include <string>


// return filename from a path
std::string getPathBaseName( const std::string path );

// return the base name of a filename
std::string getBaseFileName( std::string filename );

// load lines from input and push them into names
void load_filenames( const std::string &input, std::vector<std::string> &out );

// given a number of seconds return a human readable formatted string
std::string format_time( time_t seconds );

// get VM and RSS of the current process
void mem_usage( double& VM, double& RSS );

// get human readable formatted strings of VM and RSS of the current process
std::pair<std::string,std::string> mem_usage();

#endif	//UTILITIES_HPP

