#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

template <class T> class LinkedList;
template <class T> class ListNode;


template <class T> 
class ListNode 
{
	friend class LinkedList<T>;
	
private:
	
	T *m_val;
	ListNode<T> *m_next;
	
public:
	
	ListNode( const T &x )
	{
		this->m_val = new T(x);
		this->m_next = NULL;
	}
	
	~ListNode()
	{
		delete this->m_val;
	}
	
	inline T* p_val() { return this->m_val; }
	inline ListNode<T>* next() { return this->m_next; }
};


template <class T> 
class LinkedList
{
	
protected:
	
	ListNode<T> *m_first;
	ListNode<T> *m_last;
	size_t m_size;
	
public:
	
	LinkedList();
	LinkedList( const LinkedList<T> &l );
	~LinkedList();
	
	inline bool empty() const { return this->m_size == 0; }
	inline size_t size() const { return this->m_size; }
	
	inline ListNode<T>* back() { return this->m_last; }
	inline ListNode<T>* front() { return this->m_first; }
	
	void push_front(const T &x);
	void push_back(const T &x);
	void clear();
	
	LinkedList<T> & operator=(const LinkedList<T> & l);
};


template <class T>
LinkedList<T>::LinkedList()
{
	m_first = NULL;
	m_last = NULL;
	m_size = 0;
}


template <class T>
LinkedList<T>::LinkedList( const LinkedList<T> &l )
{
	this->m_first = NULL;
	this->m_last = NULL;
	this->m_size = 0;
	
	for( ListNode<T> *cur = l.m_first; cur != NULL; cur = cur->m_next ) 
		this->push_back( cur->m_val );
}


template <class T>
LinkedList<T>::~LinkedList()
{
	ListNode<T> *cur = this->m_first; 
	
	while( cur != NULL )
	{
		ListNode<T> *next = cur->m_next;
		delete cur;
		cur = next;
	}
}


template <class T>
void LinkedList<T>::push_front(const T &x)
{
	ListNode<T> *node = new ListNode<T>(x);
	
	if( this->empty() ) 
	{
		this->m_first = node;
		this->m_last = node;
	}
	else
	{
		node->m_next = this->m_first;
		this->m_first = node;
	}
	
	this->m_size++;
}


template <class T>
void LinkedList<T>::push_back(const T &x)
{
	ListNode<T> *node = new ListNode<T>(x);
	
	if( this->empty() ) 
	{
		this->m_first = node;
		this->m_last = node;
	}
	else
	{
		this->m_last->m_next = node;
		this->m_last = node;
	}
	
	this->m_size++;
}


template <class T>
void LinkedList<T>::clear()
{
	ListNode<T> *cur = this->m_first; 
	
	while( cur != NULL )
	{
		ListNode<T> *next = cur->m_next;
		delete cur;
		cur = next;
	}
	
	m_first = NULL;
	m_last = NULL;
	m_size = 0;
}

 
#endif	/* LINKED_LIST_HPP */