/*
 * File:   limits.hpp
 * Author: Riccardo Vicedomini
 *
 * Created on November 18, 2013, 3:02 PM
 */

#ifndef LIMITS_HPP
#define	LIMITS_HPP

#include <cstdint>

namespace limits {


// minimum k-mer length
const uint32_t MIN_KMER = 11;

// maximum k-mer length
const uint32_t MAX_KMER = 31;

// line length in output for fasta sequences
const uint32_t DEFAULT_FASTA_COLUMNS = 60;


} // namespace limits

#endif	//LIMITS_HPP

